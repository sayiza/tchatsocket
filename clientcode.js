// encryption encapsulation
var encryptedTexts = [];
var decryptedTexts = [];

var decrypt = function (password , content) {
  try {
    return sjcl.decrypt(password,content);
  } catch( e ) { return "???"; }
}

var encrypt = function (password , content) {
  try {
    return sjcl.encrypt(password,content);
  } catch( e ) { return ""; }
}

decryptAndRefresh = function (password) {
  decryptedTexts = [];
  encryptedTexts.forEach(function (e) {
    if ( ( !!password || e.text.match(/{"iv"/)) && e.username !== "SYSTEM" ) {
      decryptedTexts.push({ username : e.username , text : decrypt(password,e.text) });
    } else {
      decryptedTexts.push(e);
    }
  });

  $('#messages').html("");
  decryptedTexts.forEach(function (e) {
    $('#messages').append($('<li>').text( ( !!e.username ? e.username : 'Anon' ) + ': ' + e.text ));
  });
}

// viewmode switch between rooms and chat
var viewModeChat = function(roomname) {
  $("#idroomname").text(roomname);
  $(".viewmodechat").each( function(i,e) {$(e).show()} );
  $(".viewmoderooms").each( function(i,e) {$(e).hide()} );
  $("#idchatmsg").focus();
  decryptAndRefresh();
}

var viewModeRooms = function() {
  $(".viewmodechat").each( function(i,e) {$(e).hide()} );
  $(".viewmoderooms").each( function(i,e) {$(e).show()} );
}

// socket stuff
var socket = io();

var onChatSubmit = function (password , text , event) {
  if (!!!event || event.keyCode === 13) {
    if ( !!password ) {
      socket.emit('chat message', encrypt(password,text));
    } else {
      socket.emit('chat message', text);
    }
    $('#idchatmsg').val('');
    $("#idchatmsg").focus();
  //  console.log("chatting is on the way",event);
  }

  return false;
}

var setTheUserNameOnKeyup = function (username) {
  socket.emit('set username', username );
}

var leaveRoom = function () {
  socket.emit('leave room' , null );
  viewModeRooms();
}

var joinRoom = function (name) {
  console.info("this is the name:"+name+":end before points..");
  socket.emit('join room' , name );
  viewModeChat(name);
}

var createNewRoom = function (name,event) {
  if (!!!event || event.keyCode === 13) {
    socket.emit('create room', name );
    $('#idcreatenewroom').val('');
    viewModeChat(name);
  //  console.log("chatting is on the way",event);
  }
}

socket.on('chat message', function(username, msg){
  encryptedTexts.push({ username : username , text : msg });
  decryptAndRefresh( $('#idpasswordinput').val() );
});

socket.on('all messages', function(chatcontent){
  if (!!chatcontent && chatcontent.length > 0 ) {
    encryptedTexts = JSON.parse(JSON.stringify(chatcontent));
    decryptAndRefresh( $('#idpasswordinput').val() );
  }
});

socket.on('updaterooms', function(msg){
  console.log("updaterooms has been called",msg);
  $('#rooms').html("");
  msg.forEach( function(e) {
    if (e.name !== "lobby") {
      $('#rooms').append('<li onclick="joinRoom(\''+ e.name +'\')"><span> ▶ '
       + e.name +'</span><span style="float:right">'+e.usercount+'</span></li>');
    } else {
      $('#rooms').append('<li ><span> *** '
       + e.name +' *** </span><span style="float:right">'+e.usercount+'</span></li>');
    }
  })
});

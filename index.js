// todo: save room history on server
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var rooms = [{ name : "lobby" , usercount : 0 }];
var chatcontent = {};

var joinRoomFnc = function (newroom , socket) {
  var oldroom = socket.room;
  if (!!oldroom) {
    socket.leave(oldroom);
  }
  socket.join(newroom);
  socket.emit('chat message', 'SYSTEM' , 'you have connected to ' + newroom );
  socket.room = newroom;
  socket.broadcast.to(oldroom).emit('chat message', 'SYSTEM' , 'user ' + socket.username + ' has left this room');
  socket.broadcast.to(newroom).emit('chat message', 'SYSTEM' , 'new user ' + socket.username + ' has joined this room');
  var deleteit = -1;
  var index = -1;
  rooms.forEach( function(e) {
    index++;
    if (e.name === oldroom) {
      e.usercount = e.usercount - 1;
      if ( e.usercount === 0 && e.name !== "lobby" ) {
        deleteit = index;
      }
    }
    if (e.name === newroom) {
      e.usercount = e.usercount + 1;
    }
  });
  if ( deleteit > -1 ) {
    rooms.splice(deleteit, 1);
    delete chatcontent[oldroom];
  }
}

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.get('/jquery', function(req, res){
  res.sendFile(__dirname + '/jquery-3.1.0.js');
});

app.get('/clientcode', function(req, res){
  res.sendFile(__dirname + '/clientcode.js');
});

app.get('/sjcl', function(req, res){
  res.sendFile(__dirname + '/sjcl.js');
});

io.on('connection', function(socket){

  //first connection, send him the rooms:
  socket.room = "lobby";
  socket.join("lobby");
  rooms.forEach( function(e) {
    if (e.name === "lobby") {
      e.usercount = e.usercount + 1;
    }
  });
  io.emit('updaterooms', rooms );
  //first connection, END

  socket.on('set username', function(username) {
    socket.username = username;
    console.log("set username has been called" + socket.username);
  });

  socket.on('chat message', function(msg){
    //io.emit('chat message', msg);
    //console.log("emmiting msg in room: " + socket.room + " by: " +  socket.username + " content: " + msg);
    if ( !!!chatcontent[socket.room] ) {
      chatcontent[socket.room] = [];
    }
    chatcontent[socket.room].push({username:socket.username , text : msg });
    io.sockets["in"](socket.room).emit('chat message', socket.username , msg );
  });

  socket.on('create room', function(room) {
    var exists = false;
    rooms.forEach( function(e) {
      if (e.name === room) {
        exists = true;
      }
    });
    if (!exists) {
      rooms.push( { "name" : room , usercount : 0 } );
    }
    console.log("create room has been called");
    joinRoomFnc(room , socket);
    io.emit('updaterooms', rooms );
    io.sockets["in"](room).emit('all messages', chatcontent[room] );
    console.log("this is the chatcontent", chatcontent);
  });

  socket.on('join room', function(newroom) {
          joinRoomFnc(newroom , socket)
          io.emit('updaterooms', rooms );
          io.sockets["in"](newroom).emit('all messages', chatcontent[newroom] );
          console.log("join room has been called");
          //socket.emit('updaterooms', rooms );
      });
  socket.on('leave room', function(noargumentfornow) {
          var oldroom = socket.room;
          if (!!oldroom) {
            socket.leave(oldroom);
          }
          socket.join("lobby");
          socket.emit('chat message', 'SYSTEM' , 'you have connected to lobby' );
          socket.room = "lobby";
          socket.broadcast.to(oldroom).emit('chat message', 'SYSTEM' , 'user ' + socket.username + ' has left this room');
          socket.broadcast.to("lobby").emit('chat message', 'SYSTEM' , 'new user ' + socket.username + ' has joined the lobby');
          var deleteit = -1;
          var index = -1;
          rooms.forEach( function(e) {
            index++;
            if (e.name === oldroom) {
              e.usercount = e.usercount - 1;
              if ( e.usercount === 0 && e.name !== "lobby" ) {
                deleteit = index;
              }
            }
            if (e.name === "lobby") {
              e.usercount = e.usercount + 1;
            }

          });
          if ( deleteit > -1 ) {
            rooms.splice(deleteit, 1);
            delete chatcontent[oldroom];
          }
          io.emit('updaterooms', rooms );
          console.log("leave room has been called");
          //socket.emit('updaterooms', rooms );
      });

  socket.on('disconnect', function() {
        var deleteit = -1;
        var index = -1;
        rooms.forEach( function(e) {
          index++;
          if (e.name === socket.room) {
            e.usercount = e.usercount - 1;
            if ( e.usercount === 0 && e.name !== "lobby" ) {
              deleteit = index;
            }
          }
        });
        if ( deleteit > -1 ) {
          rooms.splice(deleteit, 1);
          delete chatcontent[socket.room];
        }
        socket.leave(socket.room);
        io.emit('updaterooms', rooms );
    });
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});
